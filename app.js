const path = require('path');
const bodyParser = require('body-parser');
const express = require('express');
const mongoose = require('mongoose');
const multer = require('multer');
const helmet = require('helmet');
const compression = require('compression');
const morgan = require('morgan');
const fs = require('fs');
// const https = require('https');

// Loading env variables
const dotEnv = require('dotenv').config();
const port = process.env.PORT || '3000';
const MONGODB_PREFIX = process.env.MONGODB_PREFIX;
const DB_USERNAME = process.env.DB_USERNAME;
const DB_ROOT_PASSWORD = process.env.DB_ROOT_PASSWORD;
const DB_HOST = process.env.DB_HOST;
const DB_NAME = process.env.DB_NAME;
const MONGODB_URL_RULE = process.env.MONGODB_URL_RULE;

const MONGODB_URL = `${MONGODB_PREFIX}://${DB_USERNAME}:${DB_ROOT_PASSWORD}@${DB_HOST}/${DB_NAME}?${MONGODB_URL_RULE}`

//routes
const feedRoutes = require('./routes/feed');
const authRoutes = require('./routes/auth');

const app = express();
// ssl / tls
// const privateKey = fs.readFileSync('server.key');
// const certificate = fs.readFileSync('server.cert');

// security for http header
app.use(helmet());
// compression handler
app.use(compression());
// log handler
const accessLogStream = fs.createWriteStream(path.join(__dirname, 'logs', 'access.log'), {flags: 'a'});
app.use(morgan('combined', {stream: accessLogStream}));

// body parsing middleware
app.use(bodyParser.json()); // application/json
// CORS Error Handling
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');

  next();
});
// image middleware parser
// filtering images
const fileFilter = (req, file, cb) => {
  if(file.mimetype === 'image/png' || file.mimetype === 'image/jpeg' || 'image/jpg'){
    // accept download
    cb(null, true);
  } else {
    cb(null, false);
  }
};
const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'images');
  },
  filename: (req, file, cb) => {
    cb(null, `${new Date().toISOString()}-${file.originalname}`);
  }
});
app.use(multer({storage: fileStorage}).single('image'));

// images static serving
app.use('/images', express.static(path.join(__dirname, 'images')));

//routes middlewares
app.use('/feed', feedRoutes);
app.use('/auth', authRoutes);

// main error handling middleware
app.use((error, req, res, next) => {
  console.log(error);
  const status = error.statusCode || 500;
  const message = error.message;
  const data = error.data;

  res.status(status).json({
    message: message,
    data: data
  });
});

mongoose.connect(MONGODB_URL, {useNewUrlParser: true, useFindAndModify: false})
  .then( result => {

    console.log('Connected to mongodb')

    const server = app.listen(port);
    // const server = https.createServer({ key: privateKey, cert: certificate});
    const io = require('./services/socket').init(server);

    console.log('server started.');

    io.on('connection', socket => {
      console.log('Socket.io client connected');
    });

  })
  .catch(err => console.log('Mongodb failed to connect!', err));

