const { validationResult } = require('express-validator');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = require('../models/user');

module.exports = {

  putSignup: async (req, res, next) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()){
      errors.statusCode = 422;
      errors.data = errors.array();
      errors.message = 'Validation failed, entered data is incorrect: ' + errors.array()[0].msg
      throw errors;
    }

    const { email,
            name,
            password } = req.body;

    try {
      const hashedPassword = await bcrypt.hash(password, 12)
      //creating new user
      const user = new User({
        name: name,
        email: email,
        password: hashedPassword
      })

      const _id = await user.save();

      res.status(201).json({
        message: 'User created successfully',
        userId:  _id
      });

    } catch (err) {;
      if(!err.statusCode){
        err.statusCode = 500
        next(err);
      }
    }

  },

  postLogin: async (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;

    try {

      const user = await User.findOne({email: email})
      if(!user){
        const error = new Error('User not found!');
        error.statusCode = 401;
        throw error;
      }

      const isAuth = await bcrypt.compare(password, user.password);

      if(!isAuth){

        const error = new Error('Wrong password provided!');
        error.statusCode = 401;
        throw error;

      }

      // password is ok
      // generate new token
      const token = jwt.sign({
        email: user.email,
        userId: user._id.toString()
      }, 'somesuperstrongsecretverysecretstring', { expiresIn: '1h' });

      // sending back the token to the client
      res.status(200).json({
        token: token,
        userId: user._id.toString()
      })

      // adapted for mocha testing (return is required for mocha aside your
      // json response)
      return;

    } catch(error){
      error.statusCode = error.statusCode ? error.statusCode : 500;
      next(error);
      // to set with mocha test (return is required for mocha aside your json
      // response)
      return error;
    }
  },

  getUserStatus: async (req, res, next) => {

    // Retrieve user
    try{
      const user = await User.findById(req.userId)
      if(!user){
        const error = new Error('User not found.');
        error.statusCode = 422;
        throw error;
      }
      res.status(200).json({
        status: user.status
      })
    } catch(err){
        const error = new Error(err);
        error.statusCode = error.statusCode ? error.statusCode : 500;
        next(error);
    }

  },

  putUserStatus: async (req, res, next) => {

    const status = req.body.status;
    const userId = req.params.userId;

    const user = await User.findById(userId)

    if(!user){
      const error = new Error('User not found.');
      error.statusCode = 422;
      throw error;
    }

    user.status = status;
    if(user.save()){
      res.status(200).json({
        message: 'Status successfully updated.'
      });
    }

  },

}
