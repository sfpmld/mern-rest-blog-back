const fs = require('fs');
const path = require('path');
const uuid = require("uuidv4");
const { validationResult } = require('express-validator');
const mongodb = require('mongodb');

// const io = require('../services/socket');

var dotenv = require('dotenv')
var dotenvExpand = require('dotenv-expand')

var myEnv = dotenv.config()
dotenvExpand(myEnv)

const ITEMS_PER_PAGE = +process.env.ITEMS_PER_PAGE;

const Post = require('../models/post');
const User = require('../models/user');

module.exports = {

  getPosts: async (req, res, next) => {

    const currentPage = req.query.page || 1;

    try {

      const totalItems = await Post.find().estimatedDocumentCount();
      const posts = await Post.find()
          .sort({createdAt: -1})
          .populate({ path: 'creator', select: 'name'})
          .skip((currentPage - 1) * ITEMS_PER_PAGE)
          .limit(ITEMS_PER_PAGE)

      res.status(200).json({
        message: 'posts fetched successfully',
        posts: posts,
        totalItems: totalItems
      });

    } catch (err) {

      const error = new Error(err);
      error.statusCode = error.statusCode ? error.statusCode : 500;
      next(error);

    }

  },

  getPost: async (req, res, next) => {
    const postId = req.params.postId;

    try {
      const post = await Post.findById(postId);
      // if not found ...
      if(!post){
        const error = new Error('Could not find post!');
        error.statusCode = 404;
        throw error;
      }
      // else if found send back data via rest protocol
      res.status(200).json({
        message: 'Post found.',
        post: post
      })

    } catch(err) {
      const error = new Error(err);
      error.statusCode = error.statusCode ? error.statusCode : 500;
      next(error);
    }


  },

  createPost: async (req, res, next) => {
    const { title, content } = req.body;
    const imageUrl = req.file.path;
    let creator;

    // Error checking
    // File not send
    if(!req.file){
      const error = new Error('No image provided');
      error.statusCode = 422;
      throw error;
    }

    // Validation Error
    const errors = validationResult(req);
    if(!errors.isEmpty()){
      const error = new Error('Validation failed, entered data is incorrect: ' + errors.array()[0].msg)
      error.statusCode = 422;
      throw error;
    }

    const post = new Post({
        title: title,
        imageUrl: imageUrl,
        content: content,
        creator: req.userId
    })
    // creating post in db
    try {
      await post.save();
      const user = await User.findById(req.userId);
      user.posts.push(post);

      // adaptation for testing in mocha
      const savedUser = await user.save();

      // // socket connection
      // io.getIO().emit('posts', {
      //   action: 'create',
      //   post: {
      //     ...post._doc,
      //     _id: req.userId,
      //     name: user.name
      //   }
      // });

      res.status(201).json({
        message: "post created successfully.",
        post: post,
        creator: {
          _id: user._id,
          name: user.name
        }
      });

      // adaptation for testing in mocha
      return savedUser;

    } catch (err) {
        console.log(err)
        if(!err.statusCode){
          err.statusCode = 500;
        }
        return next(err);
    }

  },

  updatePost: async (req, res, next) => {

    // Validation Error
    const errors = validationResult(req);
    if(!errors.isEmpty()){
      const error = new Error('Validation failed, entered data is incorrect: ' + errors.array()[0].msg)
      error.statusCode = 422;
      throw error;
    }
    const postId = req.params.postId;
    const title = req.body.title;
    const content = req.body.content;
    let imageUrl = req.body.image;

    //if image is updated too
    if(req.file){
      imageUrl = req.file.path;
    }

    if(!imageUrl){
      const error = new Error('No file picked.');
      error.statusCode = 422;
      throw error;
    }


    try {
      const post = await Post.findById(postId).populate('creator')

      if(!post){
        const error = new Error('Could not find post');
        error.statusCode = 422;
        throw error;
      }
      // Test if user is Authorzed for deleting this post
      if(post.creator._id.toString() !== req.userId){
        const error = new Error('Forbidden, user not authorized to delete this post');
        error.statusCode = 403;
        throw error;
      }
      // if image path to update, remove old imageUrl
      if(imageUrl !== post.imageUrl){
        clearImage(post.imageUrl);
      }

      post.title = title;
      post.content = content;
      post.imageUrl = imageUrl;

      const result = await post.save();

      io.getIO().emit('posts', {
        action: 'update',
        post: result
      });

      res.status(200).json({
        message: 'Post updated successfully.',
        post: post
      });

    } catch (err) {

      const error = new Error(err);
      error.statusCode = error.statusCode ? error.statusCode : 500;
      next(error);

    }


  },

  deletePost: async (req, res, next) => {

    const postId = req.params.postId;

    try {

      const post = await Post.findById(postId)

      if(!post){
        const error = new Error('Could not find post');
        error.statusCode = 422;
        throw error;
      }

      // test if user is authorized to delete this post (he must own this
      // post)
      if(post.creator.toString() !== req.userId){
        const error = new Error('Forbidden, you\'re not the owner of this post!');
        error.statusCode = 403;
        throw error;
      }

      // removing post
      const success = await Post.findByIdAndRemove(postId)
      if(success){
        // clear image
        clearImage(post.imageUrl);

        // clearing posts reference array inside user
        const user = await User.findById(req.userId);

        user.posts.pull({_id: new mongodb.ObjectID(postId)});
        await user.save();

        io.getIO().emit('posts', {
          action: 'delete',
          posts: postId
        });

        console.log('success');
        res.status(200).json({
          message: 'post successfully deleted.'
        });

      }

  } catch(err){

    const error = new Error(err);
    error.statusCode = error.statusCode ? error.statusCode : 500;
    next(error);
  }

  },

}

const clearImage = imagePath => {
  imagePath = path.join(__dirname, '..', imagePath)
  fs.unlink(imagePath, err => console.log(err))
}


