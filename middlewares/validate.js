const User = require('../models/user')

const { body, check } = require('express-validator');

module.exports = {
  createPostVal: [
    body("title")
      .isLength({ min: 5 })
      .trim(),
    body("content")
      .isLength({ min: 5 })
      .trim()
  ],
  updatePostVal: [
    body("title")
      .isLength({ min: 5 })
      .trim(),
    body("content")
      .isLength({ min: 5 })
      .trim()
  ],
  signupVal: [
    check("email")
      .isEmail()
      .withMessage("Please enter a valid email.")
      .custom((value, { req }) => {
        return User.findOne({ email: req.body.email }).then(userFound => {
          if(userFound) {
            return Promise.reject("Email address already exists.");
          }
        });
      })
      .normalizeEmail(),

    body("password")
      .trim()
      .isLength({ min: 5 }),

    body("name")
      .trim()
      .not()
      .isEmpty()
  ]
};
