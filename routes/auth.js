const express = require('express');
const router = express.Router();

const authController = require('../controllers/auth');

const validate = require('../middlewares/validate');
const isAuth = require('../middlewares/is-auth');

// GET
router.get('/status/:userId', isAuth, authController.getUserStatus);

// POST
router.post('/login', authController.postLogin);

// PUT
router.put('/signup', validate.signupVal, authController.putSignup);
router.put('/status/:userId', isAuth, authController.putUserStatus);


module.exports = router;
