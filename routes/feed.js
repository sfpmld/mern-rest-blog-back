const express = require('express');
const router = express.Router();

const validate = require('../middlewares/validate');
const isAuth = require('../middlewares/is-auth');

const feedController = require('../controllers/feed');

// GET
router.get('/posts', isAuth, feedController.getPosts);
router.get('/post/:postId', isAuth, feedController.getPost);

// POST
router.post('/post', validate.createPostVal, isAuth, feedController.createPost);

// PUT
router.put('/post/:postId', validate.createPostVal, isAuth, feedController.updatePost);

// DELETE
router.delete('/post/:postId', isAuth, feedController.deletePost);


module.exports = router;
