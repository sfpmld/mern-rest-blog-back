const authMiddleware = require('../middlewares/is-auth');
const jwt = require('jsonwebtoken');
const sinon = require('sinon');

const expect = require('chai').expect;

describe('authMiddleware mocha test block', function(){

  // we'll test if error is thrown when not receive and authorization header
  it('should throw an error if no authorization is present', function(){
    //request object that fit with our scenario
    const req = {
      get: (headerName) => null
    }
    expect(authMiddleware.bind(this, req, {}, ()=>{})).to.throw('Not Authenticated');

  });

  // we'll test if error is thrown when authorization header is not of the form of
  // 'Bearer sfsfsfsftoken'
  it('should throw an error if the authorization header is only one string', function(){
    const req = {
      get: (headerName) => 'BearerInOneStringOnly'
    }
    expect(authMiddleware.bind(this, req, {}, ()=>{})).to.throw();

  });

  // we'll test if error is thrown when not verifying token
  it('should throw an error if the token can not be verified', function(){
    const req = {
      get: (headerName) => 'Bearer tokenThatWillCertainlyFailed'
    }
    expect(authMiddleware.bind(this, req, {}, ()=>{})).to.throw();

  });

  // if token validated, we should have req.userId filled
  it('should yield a userId after decoding the token', function(){
    const req = {
      get: (headerName) => 'Bearer tokenThatWillCertainlyFailed'
    }
    // we are overwritting jwt.verify method to set our scenario
    sinon.stub(jwt, 'verify', )
    jwt.verify.returns({
      userId: "mySuperId"
    });

    authMiddleware(req, {}, ()=>{});
    expect(req).to.have.property('userId');
    expect(req).to.have.property('userId', 'mySuperId');
    expect(jwt.verify.called).to.be.true;
    jwt.verify.restore();
  });
});

